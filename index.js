class MyArray {
  constructor() {
    Object.defineProperty(this, "_length", {
      writable: true
    });

    if (arguments.length === 1 && typeof arguments[0] === "number") {
      for (let i = 0; i < arguments[0]; i += 1) {
        this[i] = null;
      }
      this._length = arguments[0];
    } else {
      for (let i = 0; i < arguments.length; i += 1) {
        this[i] = arguments[i];
      }
      this._length = arguments.length;
    }
  }

  get length() {
    let count = this._length;

    return count;
  }

  set length(value) {
    this._length = value;
  }

  push() {
    for (let i = 0; i < arguments.length; i += 1) {
      this[this.length] = arguments[i];
    }

    return this.length;
  }

  pop() {
    let el = this[this.length - 1];
    delete this[this.length - 1];

    return el;
  }

  unshift() {
    let del = arguments.length;
    let thisLength = this.length;
    let fullLength = thisLength + del;

    for (let i = fullLength - 1; i >= 0; i -= 1) {
      if (i >= del) {
        this[i] = this[i - del];
      } else {
        this[i] = arguments[i];
      }
    }
    return this.length;
  }

  shift() {
    let el = this[0];
    delete this[0];

    for (let i = 0; i < this.length; i += 1) {
      this[i] = this[i];
    }

    return el;
  }

  forEach(callback) {
    for (let i = 0; i < this.length; i += 1) {
      callback(this[i], i, this);
    }
  }

  map(callback) {
    let newArr = new MyArray();
    for (let i = 0; i < this.length; i += 1) {
      newArr.length += 1;
      newArr[i] = callback(this[i], i, this);
    }

    return newArr;
  }

  filter(callback) {
    let newArr = new MyArray();
    for (let i = 0; i < this.length; i += 1) {
      if (callback(this[i], i, this)) {
        newArr[i] = this[i];
      }
    }

    return newArr;
  }

  reduce(callback, accum = 0) {
    for (let i = 0; i < this.length; i += 1) {
      accum = callback(accum, this[i], i, this);
    }

    return accum;
  }

  toString() {
    let newStr = "";
    for (let i = 0; i < this.length; i += 1) {
      newStr += String(this[i]) + (i === this.length - 1 ? "" : ",");
    }

    return newStr;
  }

  sort(compare) {
    compare = compare || compareDefault;

    function compareDefault(a, b) {
      a = String(a);
      b = String(b);
      if (a > b) return 1;
      if (a == b) return 0;
      if (a < b) return -1;
    }

    let count = this.length - 1;
    for (let i = 0; i < count; i++) {
      for (let j = 0; j < count - i; j++) {
        if (compare(this[j], this[j + 1]) > 0) {
          let max = this[j];
          this[j] = this[j + 1];
          this[j + 1] = max;
        }
      }
    }
    return this;
  }

  static from(arrayLike, mapFn, thisArg) {
    let newArr = new MyArray();

    if (arrayLike == null) {
      throw new TypeError(
        "MyArray.from requires an array-like object - not null or undefined"
      );
    }

    if (typeof mapFn !== "undefined" && typeof mapFn !== "function") {
      let errorMsg = `${mapFn} is not a function. When provided, the second argument must be a function`;
      throw new TypeError(errorMsg);
    }

    if (typeof arrayLike === "number" && arrayLike.length === 1) {
      return newArr;
    }

    let arrayLikeObj = Object(arrayLike);

    for (let key in arrayLikeObj) {
      if (arrayLikeObj.hasOwnProperty(key)) {
        newArr.length += 1;
        if (mapFn) {
          newArr[key] = thisArg
            ? mapFn.call(thisArg, arrayLikeObj[key])
            : mapFn(arrayLikeObj[key]);
        } else {
          newArr[key] = arrayLikeObj[key];
        }
      }
    }
    return newArr;
  }
}
